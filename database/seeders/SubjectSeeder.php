<?php

namespace Database\Seeders;

use App\Models\Subject;
use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subject::create([
            'id' => 1001,
            'name' => 'Algoritma'
        ]);

        Subject::create([
            'id' => 1002,
            'name' => 'Pemrograman Dasar'
        ]);

        Subject::create([
            'id' => 1003,
            'name' => 'Basis Data'
        ]);

        Subject::create([
            'id' => 1004,
            'name' => 'Matematika Dasar'
        ]);

        Subject::create([
            'id' => 1005,
            'name' => 'Teknik Digital'
        ]);
    }
}
