<?php

namespace Database\Seeders;

use App\Models\Student;
use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Student::create([
            'id' => 1001,
            'name' => 'agus'
        ]);

        Student::create([
            'id' => 1002,
            'name' => 'budi'
        ]);

        Student::create([
            'id' => 1003,
            'name' => 'deni'
        ]);

        Student::create([
            'id' => 1004,
            'name' => 'riko'
        ]);
    }
}
