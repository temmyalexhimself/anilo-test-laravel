<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ScoreController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::group(['middleware' => ['auth:api']], function(){
    Route::get('profile', [ProfileController::class, 'index']);
});

Route::get('scores', [ScoreController::class, 'index'])->name('scores.index');
Route::get('scores/create', [ScoreController::class, 'create'])->name('scores.create');
Route::post('scores/store', [ScoreController::class, 'store'])->name('scores.store');
Route::get('scores/edit/{id}', [ScoreController::class, 'edit'])->name('scores.edit');
Route::patch('scores/update/{id}', [ScoreController::class, 'update'])->name('scores.update');
