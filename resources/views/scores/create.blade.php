@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">Input Score</div>
        <div class="card-body">
            <form action="{{ route('scores.store') }}" 
                method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="student_id">Student</label>
                    <select name="student_id" id="student_id" class="form-control @error('student_id') is-invalid @enderror">
                        <option value="">-- Select Student --</option>
                        @foreach ($students as $student)
                            <option value="{{ $student->id }}" 
                                {{ old('student_id') == $student->id ? 'selected' : '' }}>
                                {{ $student->name }}
                            </option>
                        @endforeach
                    </select>
                
                    @error('student_id')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="student_id">Subject</label>
                    <select name="subject_id" id="subject_id" class="form-control @error('subject_id') is-invalid @enderror">
                        <option value="">-- Select Subject --</option>
                        @foreach ($subjects as $subject)
                            <option value="{{ $subject->id }}" 
                                {{ old('subject_id') == $subject->id ? 'selected' : '' }}>
                                {{ $subject->name }}
                            </option>
                        @endforeach
                    </select>
                
                    @error('subject_id')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                
                <div class="form-group">
                    <label for="score">Score</label>
                    <input id="score" class="form-control @error('score') is-invalid @enderror" 
                        type="number" name="score" value="{{ old('score') }}">

                    @error('score')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
