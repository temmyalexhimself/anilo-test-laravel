@extends('layouts.app')

@section('content')
    <div class="container">
        @include('layouts.message')
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <a href="{{ route('scores.create') }}" class="btn btn-primary">Add Score</a>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
            <div class="card-body">
                <h5 class="card-title">Score List</h5>
                <table class="table table-striped" id="table-score">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Subject</th>
                            <th>Student</th>
                            <th>Score</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($scores as $score)    
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $score->subject->name }}</td>
                                <td>{{ $score->student->name }}</td>
                                <td>{{ $score->score }}</td>
                                <td>
                                    <a href="{{ route('scores.edit', $score->id) }}" class="btn btn-primary">Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js"></script>
    <script>
       $(document).ready( function () { $('#table-score').DataTable(); } );
    </script>
@endpush