<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProfileResource;
use App\Models\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ProfileController extends Controller
{
    public function index()
    {
        $users = User::orderBy('id', 'desc')->get();
        return response(ProfileResource::collection($users), Response::HTTP_OK);
    }
}
