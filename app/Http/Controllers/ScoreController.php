<?php

namespace App\Http\Controllers;

use App\Models\Score;
use App\Models\Student;
use App\Models\Subject;
use Illuminate\Http\Request;

class ScoreController extends Controller
{
    public function index()
    {
        $scores = Score::orderBy('id', 'desc')->get();
        return view('scores.index', ['scores' => $scores]);
    }

    public function create()
    {
        $students = Student::orderBy('id', 'desc')->get();
        $subjects = Subject::orderBy('id', 'desc')->get();

        return view('scores.create', [
            'students' => $students,
            'subjects' => $subjects
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'subject_id' => 'required',
            'student_id' => 'required',
            'score' => 'required|numeric',
        ]);

        Score::create([
            'subject_id' => $request->subject_id,
            'student_id' => $request->student_id,
            'score' => $request->score
        ]);

        return redirect()->route('scores.index')->with('success', 'Success');
    }

    public function edit($id)
    {
        $score = Score::find($id);
        $students = Student::orderBy('id', 'desc')->get();
        $subjects = Subject::orderBy('id', 'desc')->get();

        return view('scores.edit', [
            'score' => $score,
            'students' => $students,
            'subjects' => $subjects
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'subject_id' => 'required',
            'student_id' => 'required',
            'score' => 'required|numeric',
        ]);

        $score = Score::find($id);
        $score->update([
            'subject_id' => $request->subject_id,
            'student_id' => $request->student_id,
            'score' => $request->score
        ]);

        return redirect()->route('scores.index')->with('success', 'Success');
    }
}
